let moment = require('moment');

module.exports = (sequelize, Sequelize, DataTypes) =>  {
    const Outlet = sequelize.define(
        "outlet",
        {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
            },
            brand_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            name: {
                type: DataTypes.STRING,
            },
            picture: {
                type: DataTypes.STRING,
            },
            address: {
                type: DataTypes.INTEGER,
            },
            longitude: {
                type: DataTypes.INTEGER,
            },
            latitude: {
                type: DataTypes.INTEGER,
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                get() {
                        return moment(this.getDataValue('created_at')).format('YYYY-MM-DD h:mm:ss');
                }
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE
            }
        },
        {
            // Options
            timestamps: true,
            underscrored: true,
            freezeTableName: true,

            getterMethods:{
                like_resume: function () {
                    if (this.getDataValue("like_resume") != undefined) {
                         return this.getDataValue('like_resume').split(",");
                    } else {
                        return [];
                    }

               }
            },

            // define the table's name

            tableName: 'outlet',
            createdAt: "created_at",
            updatedAt: "updated_at"
        }
    );

      return Outlet;
}
