const config = require("../config/config");
const { Sequelize, DataTypes, Op } = require("sequelize");

const sequelize = new Sequelize(
  config.DB_NAME,
  config.DB_USER,
  config.DB_PASS,
  {
    host: config.DB_HOST,
    dialect: 'mysql',
    operatorsAliases: 0,
    poll: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.Op = Op;
db.sequelize = sequelize;

db.brand = require("./Brand")(sequelize, Sequelize, DataTypes);
db.outlet = require("./Outlet")(sequelize, Sequelize, DataTypes);
db.product = require('./Product')(sequelize, Sequelize, DataTypes);

db.outlet.belongsTo(db.brand, {
      foreignKey: "brand_id",
      targetKey: "id"
});

db.brand.hasMany(db.outlet, {
    foreignKey: "brand_id",
    targetKey: "id"
});

db.product.belongsTo(db.outlet, {
    foreignKey: "outlet_id",
    targetKey: "id"
});

db.outlet.hasOne(db.product, {
    foreignKey : "outlet_id",
    targetKey  : "id",
});

module.exports = db;
