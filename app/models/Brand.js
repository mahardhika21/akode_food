module.exports = (sequelize, Sequelize, DataTypes) => {
  const Brand = sequelize.define(
    "brand", // Model name
    {
      // Model attributes
        id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
                },
        name: {
                type: DataTypes.STRING,  
        },
        logo: {
                type: DataTypes.STRING
        },
        banner: {
                type: DataTypes.STRING
        },
        status: {
             type: DataTypes.STRING
        },
        created_at: {
                allowNull: false,
                type: DataTypes.DATE
        },
        updated_at: {
                allowNull: false,
                type: DataTypes.DATE
        }
    },
    {
            // Options
            timestamps: true,
            underscrored: true,
            freezeTableName: true,
        // define the table's name
            tableName: 'brand',
            createdAt: "created_at",
            updatedAt: "updated_at"
    }
  );

  return Brand;
};
