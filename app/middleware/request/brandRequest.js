async function store(req, res, next) {
    if (!req.body.name) {
        return res.json({
            success: false,
            messgae: "name can'nt by null",
        });
    }

    if (!req.files.logo) {
        return res.json({
            success: false,
            messgae: "logo can'nt by null",
        });
    }

    return next();
}

async function update(req, res, next) {
     if (!req.body.name) {
        return res.json({
            success: false,
            messgae: "name can'nt by null",
        });
    }
    return next();
}

module.exports = {
    store, update
}