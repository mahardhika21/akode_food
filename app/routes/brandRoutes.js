let express = require('express');
let router = express.Router();
const multer = require('multer');
const imgFilter = require('../middleware/imageFilter');
const upload = multer({ dest: 'public/tmp/', fileFilter: imgFilter.imageFIlter });
const uploadBrand = upload.fields([{name : 'logo', maxCount : 1}, {name : 'banner', maxCount : 1}])

const filterRequest = require('../middleware/request/brandRequest');
// services
let brandServices = require('../services/brandServices');


router.get('/brand', brandServices.index);
router.post('/brand', uploadBrand, filterRequest.store,brandServices.store);
router.put('/brand/:id', uploadBrand, filterRequest.update, brandServices.update);
router.get('/brand/:id', brandServices.detail);
router.delete('/brand/:id', brandServices.destroy);
router.get('/distance_test', brandServices.distance_test);

module.exports = router;