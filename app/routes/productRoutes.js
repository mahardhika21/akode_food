let express = require('express');
let router = express.Router();
const multer = require('multer');
const imgFilter = require('../middleware/imageFilter');
const upload = multer({ dest: 'public/tmp/', fileFilter: imgFilter.imageFIlter });
const uploadData = upload.fields([{ name: 'picture', maxCount: 1 }]);

const filterRequest = require('../middleware/request/productRequest');
// services list
let productServices = require('../services/productServices');
// route list
router.get('/product', productServices.index);
router.post('/product', uploadData, filterRequest.store,productServices.store);
router.put('/product/:id', uploadData, filterRequest.update, productServices.update);
router.get('/product/:id', productServices.detail);
router.delete('/product/:id', productServices.destroy);

module.exports = router;