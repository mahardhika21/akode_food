let express = require('express');
let router = express.Router();
const multer = require('multer');
const imgFilter = require('../middleware/imageFilter');
const upload = multer({ dest: 'public/tmp/', fileFilter: imgFilter.imageFIlter });
const uploadData = upload.fields([{ name: 'picture', maxCount: 1 }]);

const filterRequest = require('../middleware/request/outletRequest');
// services list
let outletServices = require('../services/outletServices');
// route list
router.get('/outlet', outletServices.index);
router.post('/outlet', uploadData, filterRequest.store,outletServices.store);
router.put('/outlet/:id', uploadData, filterRequest.update, outletServices.update);
router.get('/outlet/:id', outletServices.detail);
router.delete('/outlet/:id', outletServices.destroy);
router.get('/list_outlet_distance', outletServices.listOutleByDistance);

module.exports = router;