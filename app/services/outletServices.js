const db = require('../models');
const fs = require('fs');
const config = require('../config/config');
const path = require('path');
const Looging = require('../libraries/logging');
let { Sequelize, QueryTypes } = require('sequelize');

let sequelize = new Sequelize(
  config.DB_NAME,
  config.DB_USER,
  config.DB_PASS,
  {
    host: config.DB_HOST,
    dialect: 'mysql',
    operatorsAliases: 0,
    poll: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
  }
);
async function index(req, res) {
    try {
        if (req.query.filterBy == 'brand') {
            let brandData = await db.outlet.findAll({
                where : {brand_id : req.query.brand_id}
            });
             return res.json({
                success: true,
                message: "succes get data",
                data: brandData
            });
        } else {
            let brandData = await db.outlet.findAll();
             return res.json({
                success: true,
                message: "succes get data",
                data: brandData
            });
        }
    } catch (err) {
        Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied get data",
            log : err.message,
        });
    }
}

async function store(req, res) {
    try {
        const payload = req.body;
        let picture = await uploadFile(req.files.picture[0], 'outlet');

        await db.outlet.create({
            brand_id : payload.brand_id,
            name: payload.name,
            picture: picture.name,
            address: payload.address,
            longitude: payload.longitude,
            latitude : payload.latitude,
        });
        return res.json({
            success: true,
            message: "success store data otlet",
        });
    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied store data",
            log : err.message,
        });
    }
}

async function update(req, res) {
    try {
         let outletData = await db.outlet.findOne({
             where: {
                 id: req.params.id
             }
         });
        if (outletData == undefined) {
              return res.json({
                success: false,
                message: "outlet data not found",
            });
        }
        const payload = req.body;
        let picture = { name: outletData.picture };
        if (req.files.picture) {
            picture = await uploadFile(req.files.picture[0], 'outlet');
            if(fs.existsSync(outletData.picture)) {
                fs.unlink(outletData.picture, () => { return true });
            }
        } 

        await db.outlet.update({
            brand_id : payload.brand_id,
            name: payload.name,
            picture: picture.name,
            address: payload.address,
            longitude: payload.longitude,
            latitude : payload.latitude,
        }, {
            where : { id : outletData.id}
        });
        return res.json({
            success: true,
            message: "success update data outlet",
        });
    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied update data",
            log : err.message,
        });
    }
}

async function detail(req, res) {
    try {
         let outletData = await db.outlet.findOne({
             where: {
                 id: req.params.id
             }
        });
        return res.json({
            success: true,
            message: "succes get detail data",
            data: outletData
        });
    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied select data",
            log : err.message,
        });
    }
}

async function destroy(req, res) {
    try {
         let outletData = await db.outlet.findOne({
             where: {
                 id: req.params.id
             }
         });
        if (outletData == undefined) {
              return res.json({
                success: false,
                message: "outlet data not found",
            });
        }

        await db.outlet.destroy({
            where: { id: outletData.id }
        }).then((data) => {
            if(fs.existsSync(outletData.picture)) {
                fs.unlink(outletData.picture, () => { return true });
            }
        });

        return res.json({
            success: true,
            message: "succes get delete data",
        });

    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied destroy data",
            log : err.message,
        });
    }
}

async function uploadFile(file, dir)
{
    if (file) {
        let filename = file.originalname;
        let fileType = filename.split(".").length == 2 ? filename.split(".")[1] : "jpg";
        let newname = Date.parse(new Date()) + '-' + Math.floor(Math.random() * 999) + '.' + fileType;
        const target = path.join(__dirname, config.ASSETS + "/" + dir, newname);
        fs.renameSync(file.path, target);
        return {
            status: true,
            name: `public/img/${dir}/${newname}`
        };
    }else {
        return {
            status: false,
            name: null,
        };
    }
}

async function listOutleByDistance(req, res) {
    try {
    
        let latMy = parseFloat(req.query.lat);
        let lngMy = parseFloat(req.query.lng);
        let querySql = `SELECT brand.name AS brand,outlet.id AS outlet_id, outlet.name AS outlet, outlet.address, outlet.longitude, outlet.latitude, ROUND(( 6371 * acos( cos( radians(-6.1751521538226815) ) * cos( radians( latitude ) ) * cos( radians( outlet.longitude ) - radians(106.82712834941654) ) + sin( radians(-6.1751521538226815) ) * sin(radians(outlet.latitude)) ) ), 2) AS distance,(SELECT COUNT(*) FROM product WHERE product.outlet_id = outlet.id) AS total_product FROM outlet INNER JOIN brand ON outlet.brand_id = brand.id ORDER BY distance`;
        let queryData = await sequelize.query(querySql, { type: QueryTypes.SELECT });

         return res.json({
            success: true,
            message: "success  data",
            data : queryData
        });

    } catch(err) {
        Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied  data",
            log : err.message,
        });
    }
}

module.exports = {
    index, store, update, destroy, detail, listOutleByDistance
}