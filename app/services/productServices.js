const db = require('../models');
const fs = require('fs');
const config = require('../config/config');
const path = require('path');
const Looging = require('../libraries/logging');
let Sequelize = require('sequelize');

async function index(req, res) {
    try {
        query = {
                where : {outlet_id : req.query.outlet_id}
            };
        let brandData = await db.product.findAll(query);
        return res.json({
            success: true,
            message: "succes get data",
            data: brandData
        });
    } catch (err) {
        Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied get data",
            log : err.message,
        });
    }
}

async function store(req, res) {
    try {
        const payload = req.body;
        let picture = await uploadFile(req.files.picture[0], 'outlet');

        await db.product.create({
            outlet_id : payload.outlet_id,
            name: payload.name,
            picture: picture.name,
            price : payload.price,
            code : payload.code,
        });
        return res.json({
            success: true,
            message: "success store data otlet",
        });
    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied store data",
            log : err.message,
        });
    }
}

async function update(req, res) {
    try {
         let productData = await db.product.findOne({
             where: {
                 id: req.params.id
             }
         });
        if (productData == undefined) {
              return res.json({
                success: false,
                message: "product data not found",
            });
        }
        const payload = req.body;
        let picture = { name: productData.picture };
        if (req.files.picture) {
            picture = await uploadFile(req.files.picture[0], 'product');
            if(fs.existsSync(productData.picture)) {
                fs.unlink(productData.picture, () => { return true });
            }
        } 

        await db.product.update({
            outlet_id : payload.brand_id,
            name: payload.name,
            picture: picture.name,
            price : payload.price,
            code : payload.code,
        }, {
            where : { id : productData.id}
        });
        return res.json({
            success: true,
            message: "success update data outlet",
        });
    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied update data",
            log : err.message,
        });
    }
}

async function detail(req, res) {
    try {
         let productData = await db.product.findOne({
             where: {
                 id: req.params.id
             }
        });
        return res.json({
            success: true,
            message: "succes get detail data",
            data: productData
        });
    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied select data",
            log : err.message,
        });
    }
}

async function destroy(req, res) {
    try {
         let productData = await db.outlet.findOne({
             where: {
                 id: req.params.id
             }
         });
        if (productData == undefined) {
              return res.json({
                success: false,
                message: "product data not found",
            });
        }

        await db.product.destroy({
            where: { id: productData.id }
        }).then((data) => {
            if(fs.existsSync(productData.picture)) {
                fs.unlink(productData.picture, () => { return true });
            }
        });

        return res.json({
            success: true,
            message: "succes get delete data",
        });

    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied destroy data",
            log : err.message,
        });
    }
}

async function uploadFile(file, dir)
{
    if (file) {
        let filename = file.originalname;
        let fileType = filename.split(".").length == 2 ? filename.split(".")[1] : "jpg";
        let newname = Date.parse(new Date()) + '-' + Math.floor(Math.random() * 999) + '.' + fileType;
        const target = path.join(__dirname, config.ASSETS + "/" + dir, newname);
        fs.renameSync(file.path, target);
        return {
            status: true,
            name: `public/img/${dir}/${newname}`
        };
    }else {
        return {
            status: false,
            name: null,
        };
    }
}

module.exports = {
    index, store, update, destroy, detail
}