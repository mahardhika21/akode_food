const db = require('../models');
const fs = require('fs');
const config = require('../config/config');
const path = require('path');
const Looging = require('../libraries/logging');
let { Sequelize, QueryTypes } = require('sequelize');
let Distance = require('../libraries/calculateDistance');

let sequelize = new Sequelize(
  config.DB_NAME,
  config.DB_USER,
  config.DB_PASS,
  {
    host: config.DB_HOST,
    dialect: 'mysql',
    operatorsAliases: 0,
    poll: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
  }
);

async function index(req, res) {
    try {
        let brandData = await db.brand.findAll({
            status : 1
        });
        return res.json({
            success: true,
            message: "succes get data",
            data : brandData
        })
    } catch (err) {
        Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied get data",
            log : err.message,
        });
    }
}

async function store(req, res) {
    try {
        const payload = req.body;
        let logo = await uploadFile(req.files.logo[0], 'brand');
        let banner = null;
       // console.log(req.files.logo[0]);
        if (req.files.banner) {
            banner = await uploadFile(req.files.banner[0], 'brand');
        }

        await db.brand.create({
            name: payload.name,
            logo: logo.name,
            banner : banner != null ? banner.name : null,
        });
        return res.json({
            success: true,
            message: "success store data profile",
        });
    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied store data",
            log : err.message,
        });
    }
}

async function update(req, res) {
    try {
         let brandData = await db.brand.findOne({
             where: {
                 id: req.params.id
             }
         });
        if (brandData == undefined) {
              return res.json({
                success: false,
                message: "brand data not found",
            });
        }
        const payload = req.body;
        let logo = { name: brandData.logo };
        let banner = { name: brandData.banner };
        if (req.files.logo) {
            logo = await uploadFile(req.files.logo[0], 'brand');
            if(fs.existsSync(brandData.logo)) {
                fs.unlink(brandData.logo, () => { return true });
            }
        } 
        if (req.files.banner) {
            banner = await uploadFile(req.files.banner[0], 'brand');
            if(fs.existsSync(brandData.banner)) {
                fs.unlink(brandData.logo, () => { return true });
            }
        }

        await db.brand.update({
            name: payload.name,
            logo: logo.name,
            banner : banner != null ? banner.name : null,
        }, {
            where : { id : brandData.id}
        });
        return res.json({
            success: true,
            message: "success update data profile",
        });
    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied update data",
            log : err.message,
        });
    }
}

async function detail(req, res) {
    try {
         let brandData = await db.brand.findOne({
             where: {
                 id: req.params.id
             }
        });
        return res.json({
            success: true,
            message: "succes get detail data",
            data: brandData
        });
    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied select data",
            log : err.message,
        });
    }
}

async function destroy(req, res) {
    try {
         let brandData = await db.brand.findOne({
             where: {
                 id: req.params.id
             }
         });
        if (brandData == undefined) {
              return res.json({
                success: false,
                message: "brand data not found",
            });
        }

        await db.brand.destroy({
            where: { id: brandData.id }
        }).then((data) => {
            if(fs.existsSync(brandData.logo)) {
                fs.unlink(brandData.logo, () => { return true });
            }
            if(fs.existsSync(brandData.banner)) {
                fs.unlink(brandData.banner, () => { return true });
            }
        });

        return res.json({
            success: true,
            message: "succes get delete data",
        });

    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied destroy data",
            log : err.message,
        });
    }
}

async function uploadFile(file, dir)
{
    if (file) {
        let filename = file.originalname;
        let fileType = filename.split(".").length == 2 ? filename.split(".")[1] : "jpg";
        let newname = Date.parse(new Date()) + '-' + Math.floor(Math.random() * 999) + '.' + fileType;
        const target = path.join(__dirname, config.ASSETS + "/" + dir, newname)
        // const target = path.join(__dirname, config.ASSETS + "/accounts/premium/", file.originalname)
        fs.renameSync(file.path, target);
        //console.log(target);
        return {
            status: true,
            name: `public/img/${dir}/${newname}`
        };
    }else {
        return {
            status: false,
            name: null,
        };
    }
}

async function distance_test(req, res) {
    try {
        // let outlet = await db.outlet.findOne({
        //     where : { id : req.query.id}
        // });
        // let latMy = parseFloat(req.query.lat);
        // let lngMy = parseFloat(req.query.lng);
        // let queryData = await sequelize.query("SELECT id,name, ROUND(( 6371 * acos( cos( radians(-6.1751521538226815) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(106.82712834941654) ) + sin( radians(-6.1751521538226815) ) * sin(radians(latitude)) ) ), 2) AS distance FROM outlet ORDER BY distance", { type: QueryTypes.SELECT });

        // let distance_data = Distance(latMy, lngMy, parseFloat(outlet.latitude), parseFloat(outlet.longitude));
        //  return res.json({
        //     success: true,
        //     message: "success  data",
        //     data : 12,
        //     outlet : queryData,
        //     op : {
        //         lat_tujuan : parseFloat(outlet.latitude),
        //         lng_tujuan : parseFloat(outlet.longitude),
        //         latMy : latMy,
        //         lngMy : lngMy, 
        //         distance : distance_data.toFixed(2)
        //     },
        // });
        fs.unlink('public/img/brand/1635045094000-467.PNG', (data) => {
            console.log(data);
        })
        return res.json({
            success: true,
            message: "falied  data",
        });

    } catch(err) {
        Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied  data",
            log : err.message,
        });
    }
}
module.exports = {
    index, store, update, destroy, detail, distance_test
}