# Akode_Food

adalah api crud outlet makan yang dibuat dengan nonde/expressjs menggunakan database mysql

## Installation

clone / download projek

```bash
git clone https://gitlab.com/mahardhika21/akode_food.git
```
masuk kedalam folder projek
```bash
cd akode_food
```
 install atau tambahkan node modules
```bash
npm update
```
buat dan import database akode_food.sql, untuk setup databse di file .env
## Usage

```python
import akode_food

# dir/akode_food/
npm start
```

## License
[MIT](https://choosealicense.com/licenses/mit/)