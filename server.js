let express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    config = require('./app/config/config'),
    morgan = require('morgan'),
    router = require('express').Router(),
    db = require("./app/models");
    app = express();
const multer = require('multer');
const forms = multer();
const { json } = require('express');
//  list routes service
let brandRoutes = require('./app/routes/brandRoutes');
let outletRoutes = require('./app/routes/outletRoutes');
let productRoutes = require('./app/routes/productRoutes.js');

// connect database
db.sequelize.sync({
    force : false , // To create table if exists , so make it false
    alter : false // To update the table if exists , so make it true
}).then(() => {
 // initial(); // Just use it in development, at the first time execution!. Delete it in production
});

app.use(cors());
// app.use(forms.array()); 
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(brandRoutes);
app.use(outletRoutes);
app.use(productRoutes);
app.use(express.static(__dirname));

module.exports = app;
