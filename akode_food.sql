-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for akode_food
CREATE DATABASE IF NOT EXISTS `akode_food` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `akode_food`;

-- Dumping structure for table akode_food.brand
CREATE TABLE IF NOT EXISTS `brand` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table akode_food.brand: ~2 rows (approximately)
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
INSERT INTO `brand` (`id`, `name`, `logo`, `banner`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'McDonald\'s', 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/McDonald%27s_Golden_Arches.svg/220px-McDonald%27s_Golden_Arches.svg.png', 'asdasd', '1', '2021-10-23 14:49:45', NULL),
	(7, 'Starbucks', 'public/img/brand/1635270663000-885.png', 'public/img/brand/1635270663000-338.jpg', '1', '2021-10-24 02:27:20', '2021-10-27 00:51:03');
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;

-- Dumping structure for table akode_food.outlet
CREATE TABLE IF NOT EXISTS `outlet` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `outlet_brand_id_foreign` (`brand_id`),
  CONSTRAINT `outlet_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table akode_food.outlet: ~7 rows (approximately)
/*!40000 ALTER TABLE `outlet` DISABLE KEYS */;
INSERT INTO `outlet` (`id`, `brand_id`, `name`, `picture`, `address`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
	(1, 1, 'McDonald\'s - Hayam Wuruk', 'https://media-cdn.tripadvisor.com/media/photo-s/0b/64/dd/b9/facade.jpg', 'Jl.Hyam wuruk ', '-6.160658466838272', '106.81975241941255', '2021-10-25 01:09:46', NULL),
	(2, 1, 'McDonald\'s, Raden Saleh', 'ss', 'Jalan Raden Saleh Raya, RT.2, Cikini, Central Jakarta City, Jakarta, Indonesia', '-6.1911072066130535', '106.84278177083465', '2021-10-25 01:09:47', NULL),
	(3, 1, 'McDonald\'s Salemba Raya', 's', 'Jalan Salemba Raya, RW.5, Paseban, Central Jakarta City, Jakarta, Indonesia', '-6.194866797408493', '106.85019047211837', '2021-10-25 01:09:48', NULL),
	(4, 1, 'McDonald\'s Senayan Trade Center', 'd', 'Senayan Trade Center (STC), Lt.LG, Jl. Asia Afrika Pintu IX, RT.1/RW.3, Senayan, Kecamatan Tanah Abang, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10270', '-6.257255972556795', '106.81455336794608', '2021-10-25 01:09:48', '2021-10-25 02:28:12'),
	(5, 1, 'McDonald\'s Senayan Trade Center', 'public/img/outlet/1635102102000-109.jpg', 'Senayan Trade Center (STC), Lt.LG, Jl. Asia Afrika Pintu IX, RT.1/RW.3, Senayan, Kecamatan Tanah Abang, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10270', '-6.220718134489993', '106.79785680944518', '2021-10-25 02:01:42', '2021-10-25 02:28:29'),
	(6, 7, 'Starbucks Filateli', 'public/img/outlet/1635272244000-748.jpg', 'Starbucks Filateli, Jl. Pos No.2, Pasar Baru, Sawah Besar, Central Jakarta City, Jakarta 10710, Indonesia', '-6.1663380144465725', '106.83384443373627', '2021-10-27 01:17:24', '2021-10-27 01:20:40');
/*!40000 ALTER TABLE `outlet` ENABLE KEYS */;

-- Dumping structure for table akode_food.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `outlet_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `product_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `outlet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table akode_food.product: ~4 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `outlet_id`, `name`, `picture`, `price`, `created_at`, `updated_at`) VALUES
	(2, 1, 'Big Mac', 'public/img/product/1635273389000-150.png', '36000', '2021-10-25 03:25:16', '2021-10-27 01:36:29'),
	(3, 1, 'Triple Burger with Cheese', 'public/img/product/1635273575000-61.png', '47000', '2021-10-26 23:05:01', '2021-10-27 01:39:35'),
	(4, 6, 'kopi', 'public/img/outlet/1635273260000-574.jpg', '50000', '2021-10-27 01:34:20', '2021-10-27 01:34:20'),
	(5, 2, 'Big Mac', 'public/img/outlet/1635273429000-216.png', '36000', '2021-10-27 01:37:09', '2021-10-27 01:37:09');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
